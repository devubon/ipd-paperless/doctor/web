import { Component } from '@angular/core';
import { ActivatedRoute, Event, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { LibService } from '../../../shared/services/lib.service';
import * as _ from 'lodash';



interface OptionItems {
  name: string;
  id: string;


}
@Component({
  selector: 'app-progressnote',
  templateUrl: './progressnote.component.html',
  styleUrls: ['./progressnote.component.css']
})


export class ProgressnoteComponent {

  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  item: any = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  queryParamsData: any;


  // declare option click

  optionSubjectiveSelected: any = [];
  optionObjectiveSelected: any = [];
  optionAssessmentSelected: any = [];
  optionPlanSelected: any = [];
  ////////////////////////////////////////////

  optionSubjective: any = [];
  optionsAssessment: any = [];
  optionObjective: any = [];
  optionPlan: any = [];

  dxData: any = [];
  dxName: any;
  allChecked = true;
  indeterminate = true;
  checkOptionsOne = [
    { label: 'Apple', value: 'Apple', checked: true },
    { label: 'Pear', value: 'Pear', checked: false },
    { label: 'Orange', value: 'Orange', checked: false }
  ];

  progessNote: any = [];
  showProgressNote: boolean = false;
  showDx: boolean = true;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,
    private libService: LibService,
    // private nzButtonSize:NzButtonSize


  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
  }



  ngOnInit(): void {

    this.getList();
    //this.getProgresnote();
    this.getDx();
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  // updateAllChecked(data:any) {
  //   console.log('up');
  //   console.log(data);


  // }
  updateAllChecked(e: Event): void {
    console.log(e);
    // console.log(value);
    // this.indeterminate = false;
    // if (this.allChecked) {
    //   this.checkOptionsOne = this.checkOptionsOne.map(item => ({
    //     ...item,
    //     checked: true
    //   }));
    // } else {
    //   this.checkOptionsOne = this.checkOptionsOne.map(item => ({
    //     ...item,
    //     checked: false
    //   }));
    // }
  }

  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }



  optionSubjectClick(e: any) {
    if (e.target.checked) {
      this.optionSubjectiveSelected.push(e.target.value);
    } else {
      this.optionSubjectiveSelected.splice(this.optionSubjectiveSelected.indexOf(e.target.value), 1);
      // this.optionSubjectiveSelected.pop(e.target.value);
    }

    console.log(this.optionSubjectiveSelected);
  }

  optionObjectClick(e: any) {
    if (e.target.checked) {
      this.optionObjectiveSelected.push(e.target.value);
    } else {
      this.optionObjectiveSelected.splice(this.optionObjectiveSelected.indexOf(e.target.value), 1);
      // this.optionSubjectiveSelected.pop(e.target.value);
    }

    console.log(this.optionObjectiveSelected);
  }

  optionAssessmentClick(e: any) {
    if (e.target.checked) {
      this.optionAssessmentSelected.push(e.target.value);
    } else {
      this.optionAssessmentSelected.splice(this.optionAssessmentSelected.indexOf(e.target.value), 1);
      // this.optionSubjectiveSelected.pop(e.target.value);
    }

    console.log(this.optionAssessmentSelected);
  }

  optionPlanClick(e: any) {
    if (e.target.checked) {
      this.optionPlanSelected.push(e.target.value);
    } else {
      this.optionPlanSelected.splice(this.optionPlanSelected.indexOf(e.target.value), 1);
      // this.optionSubjectiveSelected.pop(e.target.value);
    }

    console.log(this.optionPlanSelected);
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  listPatient() {
    this.router.navigate(['/ward']);
  }

  dxCode: any;

  async dxlistClick(id: any, name: any) {

    console.log(id);

    const messageId = this.message.loading('Loading...').messageId;
    try {


      let response = await this.libService.getStanding(id);
      //  let progessnoteData: any = response.data.data;
      // this.optionsDrugsUsage = response.data.data;
      console.log(response);
      this.dxName = name;
      let progessnoteData = response.data.data;
      this.optionSubjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'S')
      this.optionObjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'O')
      this.optionsAssessment = progessnoteData.filter((item: any) => item.progress_note_type_code == 'A')
      this.optionPlan = progessnoteData.filter((item: any) => item.progress_note_type_code == 'P')


      this.showProgressNote = true;
      this.showDx = false;
      this.message.remove(messageId);

      // this.optionSubjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'S')
      // this.optionObjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'O')
      // this.optionsAssessment = progessnoteData.filter((item: any) => item.progress_note_type_code == 'A')
      // this.optionPlan = progessnoteData.filter((item: any) => item.progress_note_type_code == 'P')

    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error("getLabItems" + `${error.code} - ${error.message}`);
    }

  }


  async saveProgressNote() {
    const format1 = 'YYYY-MM-DD HH:mm:ss';
    const format2 = 'YYYY-MM-DD';
    const format3 = 'HH:mm:ss';
    var date1 = new Date('2020-06-24 22:57:36');
    var date2 = new Date();
    var date3 = new Date();
    // var test =sub.id;
    //  console.log("saveProgressNote="+test);
    let data = {
      doctor_order: {
        admit_id: this.queryParamsData,
        doctor_order_date: "",
        doctor_order_time: "",
        doctor_order_by: this.queryParamsData,
        is_confirm: true,
      }
    };
    try {


      this.doctorService.saveProgressNote(data);
    } catch (error) {
      console.log("saveProgressNote=" + error);
    }
  }


  async getProgresnote() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let response = await this.libService.getProgressNote();
      let progessnoteData: any = response.data.data;

      // this.optionsDrugsUsage = response.data.data;

      //  console.log(this.optionsDrugs);
      this.message.remove(messageId);

      this.optionSubjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'S')
      this.optionObjective = progessnoteData.filter((item: any) => item.progress_note_type_code == 'O')
      this.optionsAssessment = progessnoteData.filter((item: any) => item.progress_note_type_code == 'A')
      this.optionPlan = progessnoteData.filter((item: any) => item.progress_note_type_code == 'P')

    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error("getLabItems" + `${error.code} - ${error.message}`);
    }
  }


  async getDx() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let response = await this.libService.getDx();
      let _dxData = response.data.data;  
      this.dxData =   _.sortBy(_dxData,
        [function (o) { return o.name; }]);
      console.log(this.dxData);
      this.message.remove(messageId);

    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error("getLabItems" + `${error.code} - ${error.message}`);
    }
  }
  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getWaiting(_limit, _offset);

      const data: any = response.data;
      console.log(data);


      this.total = data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) : '';
        v.admit_date = date;
        return v;
      });
      console.log(this.dataSet);

      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
}

