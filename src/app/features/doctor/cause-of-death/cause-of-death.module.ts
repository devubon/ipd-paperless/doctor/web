import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CauseOfDeathRoutingModule } from './cause-of-death-routing.module';
import { CauseOfDeathComponent } from './cause-of-death.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgZorroModule } from 'src/app/ng-zorro.module';


@NgModule({
  declarations: [
    CauseOfDeathComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    CauseOfDeathRoutingModule
  ]
})
export class CauseOfDeathModule { }
