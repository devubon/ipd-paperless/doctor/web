import { Component } from '@angular/core';
import { VariableShareService } from '../../core/services/variable-share.service';
import { Subscription } from 'rxjs';
import { NzMenuThemeType } from 'ng-zorro-antd/menu';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent {
  [x: string]: any;
  isCollapsed: boolean = false; //สั่งให้เปิด
  private subscription?: Subscription;
  user_login_name: any;
  user_id: any
  theme:NzMenuThemeType = 'light';


  constructor(
    private variableShareService: VariableShareService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.user_login_name = sessionStorage.getItem('userLoginName');
    console.log('doctor');
    // this.subscription = this.variableShareService.sharedData$.subscribe(value => {
    //   this.isCollapsed = value;
    // });
    this.subscription = this.variableShareService. sharedDataTheme$.subscribe(value => {     
      this.theme = value;
    });

  }
  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
  closeSidebar(): void {
    // this.isCollapsed = !this.isCollapsed;
    this.isCollapsed = true;
  }
  executeParentMethod(): void {
    // This method will be triggered when the child emits the event
    this.closeSidebar();
    console.log('Parent method executed.');
  }

  getCloseSidebar(): void {
    this.isCollapsed = this.variableShareService.getCloseSidebar();
    console.log(this.isCollapsed);
  }
  getTheme(): void {
    this.theme = this.variableShareService.getTheme();
    console.log(this.theme);
  }
  navigateHome(){
  
    this.closeSidebar();
    this.router.navigate(['/doctor/']);

  }
  logOut() {
    console.log('logOut');
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

}
