import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmissionnoteComponent } from './admissionnote.component';

const routes: Routes = [{ path: '', component: AdmissionnoteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdmissionnoteRoutingModule { }
