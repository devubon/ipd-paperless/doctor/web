import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WaitingRoutingModule } from './waiting-routing.module';
import { WaitingComponent } from './waiting.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    WaitingComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,
    WaitingRoutingModule
  ]
})
export class WaitingModule { }
