import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NurseComponent } from './nurse.component';
import { nurseWaitingGuard } from '../../core/guard/nurse-waiting.guard';

const routes: Routes = [
  {
    path: '',
    component: NurseComponent,
    children: [
      {
        path: '', redirectTo: 'main', pathMatch: 'full'
      },
      {
        path: 'main',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule)
      },
      {
        path: 'waiting',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./waiting/waiting.module').then(m => m.WaitingModule)
      },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseRoutingModule { }
