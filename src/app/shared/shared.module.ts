import { NgModule } from '@angular/core';
import { FooterComponent } from './footer/footer.component';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

// import { HeaderComponent } from './header/header.component';
import {SidebarComponent} from './sidebar/sidebar.component';


import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    FooterComponent,
    // HeaderComponent,
    SidebarComponent,


  ],
  imports: [
   
    RouterModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    NgxSpinnerModule
  ],
  exports: [
    FooterComponent,
    // HeaderComponent,
    SidebarComponent,


    FormsModule,
    NgxSpinnerModule
  ]
})
export class SharedModule { }
