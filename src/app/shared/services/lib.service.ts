import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LibService {

  pathPrefixLookup: any = `:40013/lookup`
  pathPrefixDoctor: any = `:40014/doctor`
  pathPrefixAuth: any = `:40010/auth`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  });

  constructor () {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async getWard() {
    const url = `/ward`;
    return this.axiosInstance.get(url);
  }

  async getProgressNote() {
    const url = `/standing-progress-note`
    return await this.axiosInstance.get(url);
  }
  async getStanding(dxId: any) {
    //const url = `/standing-progress-note/infoGroupDiseaseID/${dxId}`;
    const url = `/standing-progress-note/infoGroupDiseaseID/0181555f-a78b-486b-bdc4-5e07e6dfa38a`;
   
    console.log(url);
    
    return await this.axiosInstance.get(url);
  }

  async getLastSendings(start: any, end: any) {
    const url = `/last-sending?start=${start}&end=${end}`;
    return this.axiosInstance.get(url);
  }

  async getDx() {
    const url = `/group-disease`
    return await this.axiosInstance.get(url);
  }

}
